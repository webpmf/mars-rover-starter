package com.afs.tdd;

public enum Direction {
    North, East, West, South;
}
