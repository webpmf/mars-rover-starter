package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


public class MarsRoverTest {
    @Test
    void should_only_plus_LocationY_when_executeCommand_given_location_and_command_Move() {
        //given
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Move);

        //then
        Assertions.assertEquals(0,location.getLocationX());
        Assertions.assertEquals(1,location.getLocationY());
        Assertions.assertEquals(Direction.North,location.getDirection());

    }

    @Test
    void should_only_plus_LocationX_when_executeCommand_given_location_and_command_Move() {
        //given
        Location location = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Move);

        //then
        Assertions.assertEquals(1,location.getLocationX());
        Assertions.assertEquals(0,location.getLocationY());
        Assertions.assertEquals(Direction.East,location.getDirection());

    }

    @Test
    void should_only_minus_LocationX_when_executeCommand_given_location_and_command_Move() {
        //given
        Location location = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Move);

        //then
        Assertions.assertEquals(-1,location.getLocationX());
        Assertions.assertEquals(0,location.getLocationY());
        Assertions.assertEquals(Direction.West,location.getDirection());

    }

    @Test
    void should_only_minus_LocationY_when_executeCommand_given_location_and_command_Move() {
        //given
        Location location = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.Move);

        //then
        Assertions.assertEquals(0,location.getLocationX());
        Assertions.assertEquals(-1,location.getLocationY());
        Assertions.assertEquals(Direction.South,location.getDirection());

    }


    @Test
    void should_only_change_direction_when_executeCommand_given_location_and_command_TurnLeft() {
        //given
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.TurnLeft);

        //then
        Assertions.assertEquals(0,location.getLocationX());
        Assertions.assertEquals(0,location.getLocationY());
        Assertions.assertEquals(Direction.West,location.getDirection());

    }

    @Test
    void should_only_change_direction_when_executeCommand_given_location_west_and_command_TurnLeft() {
        //given
        Location location = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.TurnLeft);

        //then
        Assertions.assertEquals(0,location.getLocationX());
        Assertions.assertEquals(0,location.getLocationY());
        Assertions.assertEquals(Direction.South,location.getDirection());

    }

    @Test
    void should_only_change_direction_when_executeCommand_given_location_south_and_command_TurnLeft() {
        //given
        Location location = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.TurnLeft);

        //then
        Assertions.assertEquals(0,location.getLocationX());
        Assertions.assertEquals(0,location.getLocationY());
        Assertions.assertEquals(Direction.East,location.getDirection());

    }

    @Test
    void should_only_change_direction_when_executeCommand_given_location_east_and_command_TurnLeft() {
        //given
        Location location = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.TurnLeft);

        //then
        Assertions.assertEquals(0,location.getLocationX());
        Assertions.assertEquals(0,location.getLocationY());
        Assertions.assertEquals(Direction.North,location.getDirection());

    }

    @Test
    void should_only_change_direction_when_executeCommand_given_location_east_and_command_TurnRight() {
        //given
        Location location = new Location(0,0,Direction.East);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.TurnRight);

        //then
        Assertions.assertEquals(0,location.getLocationX());
        Assertions.assertEquals(0,location.getLocationY());
        Assertions.assertEquals(Direction.South,location.getDirection());

    }

    @Test
    void should_only_change_direction_when_executeCommand_given_location_south_and_command_TurnRight() {
        //given
        Location location = new Location(0,0,Direction.South);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.TurnRight);

        //then
        Assertions.assertEquals(0,location.getLocationX());
        Assertions.assertEquals(0,location.getLocationY());
        Assertions.assertEquals(Direction.West,location.getDirection());

    }

    @Test
    void should_only_change_direction_when_executeCommand_given_location_west_and_command_TurnRight() {
        //given
        Location location = new Location(0,0,Direction.West);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.TurnRight);

        //then
        Assertions.assertEquals(0,location.getLocationX());
        Assertions.assertEquals(0,location.getLocationY());
        Assertions.assertEquals(Direction.North,location.getDirection());

    }

    @Test
    void should_only_change_direction_when_executeCommand_given_location_north_and_command_TurnRight() {
        //given
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);

        //when
        marsRover.executeCommand(Command.TurnRight);

        //then
        Assertions.assertEquals(0,location.getLocationX());
        Assertions.assertEquals(0,location.getLocationY());
        Assertions.assertEquals(Direction.East,location.getDirection());

    }

    @Test
    void should_location_when_executeCommand_given_location_and_commands() {
        //given
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover = new MarsRover(location);

        //when
        List<Command> commands = new ArrayList<>();
        commands.add(Command.Move);
        commands.add(Command.TurnLeft);
        commands.add(Command.Move);
        commands.add(Command.TurnRight);
        marsRover.executeCommands(commands);

        //then
        Assertions.assertEquals(-1,location.getLocationX());
        Assertions.assertEquals(1,location.getLocationY());
        Assertions.assertEquals(Direction.North,location.getDirection());

    }


}
