package com.afs.tdd;


import java.util.List;

public class MarsRover {
    private Location location;
    private List<Command> commands;

    public MarsRover(Location location) {
        this.location = location;
    }


    public void executeCommand(Command commond) {
        if (commond == Command.Move) {
            switch (location.getDirection()) {
                case North:
                    location.setLocationY(location.getLocationY() + 1);
                    break;
                case East:
                    location.setLocationX(location.getLocationX() + 1);
                    break;
                case West:
                    location.setLocationX(location.getLocationX() - 1);
                    break;
                case South:
                    location.setLocationY(location.getLocationY() - 1);
                    break;
            }
        }
        if (commond == Command.TurnLeft) {
            switch (location.getDirection()) {
                case North:
                    location.setDirection(Direction.West);
                    break;
                case West:
                    location.setDirection(Direction.South);
                    break;
                case South:
                    location.setDirection(Direction.East);
                    break;
                case East:
                    location.setDirection(Direction.North);
                    break;
            }
        }
        if (commond == Command.TurnRight) {
            switch (location.getDirection()) {
                case East:
                    location.setDirection(Direction.South);
                    break;
                case South:
                    location.setDirection(Direction.West);
                    break;
                case West:
                    location.setDirection(Direction.North);
                    break;
                case North:
                    location.setDirection(Direction.East);
                    break;

            }
        }
    }

    public void executeCommands(List<Command> commands) {
        commands.forEach(command -> {
            executeCommand(command);
        });
    }
}
